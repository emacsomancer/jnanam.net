                   Rig-Veda for MS-WORD 97

                                                     Hiroshi KUMAMOTO
                                                     University of Tokyo
                                                     January 21, 1998

These files are based on the DOS text on the diskette accompanying
RIG VEDA, A METRICALLY RESTORED TEXT, edited by Barend A. van Nooten
and Gary B. Holland, Harvard Oriental Series, Vol. 50, Harvard UP, 1994.

The only difference is that the character 160 (A0h) for "a acute" is 
replaced by 225 (E1h) throughout. This is necessary because A0h is 
reserved by MS-WORD for "Nonbreaking space".

In order to use these WORD files, the truetype font SANSKR.TTF must be 
installed. This font is designed according to the "International 
Codepage for Sanskrit diacritics (including the special characters used
in the metrical version of the RV)". See the last page of the "Contents
of the diskette" by van Nooten. Here again for the reason stated above
"a acute" appears at 225 (E1h).

Since the lower part of this Codepage is the DOS codepage 437, and not 
the "Windows Character Set" (thus "n tilde" is 164, not 241), the Windows
users will have to reassign most of the shortcut keys for the accented 
and umlauted characters when using this font.

The main reason for preparing these WORD files and SANSKR.TTF has been 
to facilitate the use of the important electronic text on the computers
running the Japanese OS. The original DOS text of van Nooten and Holland
has been practically unusable for the average users (of DOS, WINDOWS or
MAC) here because much of the upper half of the codepage is reserved for
the Japanese characters. The text with font attribute can be an answer to
the problem.

It is hoped that italic, bold, bold-italic styles of SANSKR.TTF will be 
ready in the near future.

Needless to say, these files are for non-commercial use only. Users are 
allowed to download and use the text for their personal use, provided 
that refrence is made to the HOS edition.

At the suggestion of Professor Michael Witzel, the editor of the Series, 
I have withdrawn "for the moment" the new metrical text, and replaced it 
by the Samhita Text, which is traditionally used and still of use.

