
DECLARE SUB edgemove (board!(), x!, y!, path!, dir!)
DECLARE SUB compblack2 (board!(), a!, b!, c!, d!, mist!, kingx!, kingy!)
DECLARE SUB compwhite2 (board!(), a!, b!, c!, d!, mist!, kingx!, kingy!)
DECLARE SUB attack (board!(), x!, y!, path!, dir!)
DECLARE SUB mouseshow ()
DECLARE SUB mousehide ()
DECLARE SUB compblack (board!(), a!, b!, c!, d!, mist!, kingx!, kingy!)
DECLARE SUB compwhite (board!(), a!, b!, c!, d!, mist!, kingx!, kingy!)
DECLARE SUB Choice (board!(), mist!, a!, b!, c!, d!, colour!, win!, kingx!, kingy!, blp!, whp!, knight, escape)
DECLARE SUB Change (board!(), mist!, a!, b!, c!, d!, colour!, win!, kingx, kingy, blp, whp)
DECLARE SUB move (board(), mist, a, b, c, d, colour, win, kingx, kingy)
DECLARE SUB Drawboard (board(), blp, whp, la, lb, lc, ld, casualx(), casualy(), casn)
DECLARE SUB MouseDriver (AX%, bx%, CX%, DX%, lb%, RB%, EX%)
DIM SHARED board(10, 10), routx(1000), routy(1000), wlevel, blevel, mist, a, b, c, d, colour, win, kingx, kingy, blp, whp, knight, mouse$, escape, la(9), lb(9), lc(9), ld(9), casualx(2), casualy(2), casn, path, rcount
TIMER ON
MouseDriver 1, bx%, CX%, DX%, lb%, RB%, 1
DATA 55,89,E5,8B,5E,0C,8B,07,50,8B,5E,0A,8B,07,50,8B
DATA 5E,08,8B,0F,8B,5E,06,8B,17,5B,58,1E,07,CD,33,53
DATA 8B,5E,0C,89,07,58,8B,5E,0A,89,07,8B,5E,08,89,0F
DATA 8B,5E,06,89,17,5D,CA,08,00
7
mouseshow
mousehide
casn = -1
REM 0=empty space
REM 1=white piece
REM 2=black piece
REM 3=king
REM 4=corner 'dark' square
REM 5=centre 'dark' square
REM 6=king on centre square
win = 0
kingx = 5
kingy = 5
blp = 12
whp = 24
la(9) = -1
la(0) = 0
lc(0) = 0
lb(0) = 0
ld(0) = 0
FOR i = 0 TO 10
        FOR j = 0 TO 10
                board(i, j) = 0
        NEXT j
NEXT i
board(0, 0) = 4
board(0, 10) = 4
board(10, 0) = 4
board(10, 10) = 4
board(0, 3) = 1
board(0, 4) = 1
board(0, 5) = 1
board(0, 6) = 1
board(0, 7) = 1
board(1, 5) = 1
board(3, 0) = 1
board(3, 5) = 2
board(3, 10) = 1
board(4, 0) = 1
board(4, 4) = 2
board(4, 5) = 2
board(4, 6) = 2
board(4, 10) = 1
board(5, 0) = 1
board(5, 1) = 1
board(5, 3) = 2
board(5, 4) = 2
board(5, 5) = 6
board(5, 6) = 2
board(5, 7) = 2
board(5, 9) = 1
board(5, 10) = 1
board(6, 0) = 1
board(6, 4) = 2
board(6, 5) = 2
board(6, 6) = 2
board(6, 10) = 1
board(7, 0) = 1
board(7, 5) = 2
board(7, 10) = 1
board(9, 5) = 1
board(10, 3) = 1
board(10, 4) = 1
board(10, 5) = 1
board(10, 6) = 1
board(10, 7) = 1
colour = 2
CLS
PRINT "---------------------------------------"
PRINT "CYNINGTAFL"
PRINT "(King's Tables)"
PRINT
PRINT "Version 1.0--1999"
PRINT "Freeware - distribute freely"
PRINT "(but kindly DO NOT alter source code)"
PRINT
PRINT "by"
PRINT "Benjamin SLADE"
PRINT ""
PRINT "e-mail: SLADE@JHU.EDU"
PRINT
PRINT
PRINT "see TAFL.txt for rules/instructions"
PRINT
PRINT "---------------------------------------"
PRINT
PRINT
PRINT "No. of Players?"
PRINT "-------------------"
PRINT "1.........Two-Player"
PRINT "2.........Computer Opponent"
17
INPUT complay
IF complay = 1 THEN
        knight = 0
ELSEIF complay = 2 THEN
        PRINT ""
        PRINT "You choose to play:"
        PRINT "-----------------------------------------"
        PRINT "              1......Black (King's Men)"
        PRINT "              2......White (Attackers/Ambushers)"
        PRINT "              3......None (Computer v. Computer)"
56
        INPUT knight
        IF knight < -1 OR knight > 3 THEN GOTO 56
        IF knight = 1 OR knight = 3 THEN
                PRINT
                PRINT "Choose level of white play: 1 or 2"
57
                INPUT wlevel
                IF wlevel < 1 OR wlevel > 2 THEN GOTO 57
        END IF
        IF knight = 2 OR knight = 3 THEN
                PRINT
                PRINT "Choose level of black play: 1 or 2"
58
                INPUT blevel
                IF blevel < 1 OR blevel > 2 THEN GOTO 58
        END IF
ELSE
        GOTO 17
END IF
101
escape = 0
WHILE win = 0 AND escape = 0
        Choice board(), mist, a, b, c, d, colour, win, kingx, kingy, blp, whp, knight, escape
WEND
IF win = 1 THEN
        Drawboard board(), blp, whp, la, lb, lc, ld, casualx(), casualy(), casn
        PRINT
        PRINT "White wins!"
ELSEIF win = 2 THEN
        Drawboard board(), blp, whp, la, lb, lc, ld, casualx(), casualy(), casn
        PRINT
        PRINT "Black wins!"
ELSEIF win = 3 THEN
        Drawboard board(), blp, whp, la, lb, lc, ld, casualx(), casualy(), casn
        PRINT
        PRINT "Draw...."
END IF
IF escape <> 0 THEN
        PRINT "Game aborted."
END IF
PRINT
PRINT "1.....New Game"
PRINT "2.....Quit"
IF win = 0 THEN
        PRINT "3.....Continue"
REM        PRINT "4.....Take back moves"
END IF
99
INPUT newquit
IF newquit < 1 OR newquit > 3 THEN GOTO 99
IF newquit = 1 THEN GOTO 7
IF newquit = 3 AND win = 0 THEN
        GOTO 101
ELSEIF newquit = 3 AND win > 0 THEN
        GOTO 99
END IF
IF newquit = 4 AND win = 0 THEN
        IF la(1) < 0 AND knight > 0 THEN
                PRINT "Not possible."
                GOTO 99
        ELSEIF la(0) < 0 THEN
                PRINT "Not possible."
                GOTO 99
        ELSE
                tempboard = board(la(0) - 1, lb(0) - 1)
                board(la(0) - 1, lb(0) - 1) = board(lc(0) - 1, ld(0) - 1)
                board(lc(0) - 1, ld(0) - 1) = tempboard
                IF knight > 0 THEN
                        tempboard = board(la(1) - 1, lb(1) - 1)
                        board(la(1) - 1, lb(1) - 1) = board(lc(1) - 1, ld(1) - 1)
                        board(lc(1) - 1, ld(1) - 1) = tempboard
                        FOR h = 8 TO 0 STEP -1
                                la(h) = la(h + 1)
                                lb(h) = lb(h + 1)
                                lc(h) = lc(h + 1)
                                ld(h) = ld(h + 1)
                        NEXT h
                ELSE
                        IF colour = 1 THEN
                                colour = 2
                        ELSE
                                colour = 1
                        END IF
                END IF
                FOR h = 8 TO 0 STEP -1
                      la(h) = la(h + 1)
                      lb(h) = lb(h + 1)
                      lc(h) = lc(h + 1)
                      ld(h) = ld(h + 1)
                NEXT h
                GOTO 101
        END IF
ELSEIF newquit = 4 AND win > 0 THEN
        GOTO 99
END IF
END

SUB attack (board(), x, y, path, dir)
DIM choic(50)
path = -1
faill = 0
count = -1
i = x + 1
WHILE faill = 0
        IF i < 11 AND dir = 1 THEN
                IF board(i, y) = 1 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(i, y) = 0 OR board(i, y) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = i
                        routy(rcount) = y
                        i = i + 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
faill = 0
i = x - 1
WHILE faill = 0
        IF i > -1 AND dir = 1 THEN
                IF board(i, y) = 1 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(i, y) = 0 OR board(i, y) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = i
                        routy(rcount) = y
                        i = i - 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
faill = 0
i = y + 1
WHILE faill = 0
        IF i < 11 AND dir = 0 THEN
                IF board(x, i) = 1 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(x, i) = 0 OR board(x, i) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = x
                        routy(rcount) = i
                        i = i + 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
faill = 0
i = y - 1
WHILE faill = 0
        IF i > -1 AND dir = 0 THEN
                IF board(x, i) = 1 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(x, i) = 0 OR board(x, i) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = x
                        routy(rcount) = i
                        i = i - 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
IF count > -1 THEN
        RANDOMIZE TIMER
        rt% = INT(RND * count)
        temp = rt%
        path = choic(temp)
END IF
END SUB

SUB Change (board(), mist, a, b, c, d, colour, win, kingx, kingy, blp, whp)
casn = -1
IF board(a, b) = 3 OR board(a, b) = 6 THEN
        kingx = c
        kingy = d
END IF
IF board(c, d) = 5 THEN
        board(c, d) = 6
        GOTO 10
END IF
IF board(a, b) = 6 THEN
        board(c, d) = 3
        GOTO 10
END IF
board(c, d) = board(a, b)
10 IF board(a, b) = 6 THEN board(a, b) = 5 ELSE board(a, b) = 0
IF colour = 1 THEN
        IF (d + 1) < 11 THEN
                IF board(c, d + 1) = 2 AND (d + 2) < 11 THEN
                        IF board(c, d + 2) = 1 OR board(c, d + 2) = 4 OR board(c, d + 2) = 5 OR board(c, d + 2) = 6 THEN
                                board(c, d + 1) = 0
                                blp = blp - 1
                                casn = casn + 1
                                casualx(casn) = c + 1
                                casualy(casn) = d + 2
                        END IF
                END IF
        END IF
        IF (d - 1) > 0 THEN
                IF board(c, d - 1) = 2 AND (d - 2) > -1 THEN
                        IF board(c, d - 2) = 1 OR board(c, d - 2) = 4 OR board(c, d - 2) = 5 OR board(c, d - 2) = 6 THEN
                                board(c, d - 1) = 0
                                blp = blp - 1
                                casn = casn + 1
                                casualx(casn) = c + 1
                                casualy(casn) = d
                        END IF
                END IF
        END IF
        IF (c + 1) < 11 THEN
                IF board(c + 1, d) = 2 AND (c + 2) < 11 THEN
                        IF board(c + 2, d) = 1 OR board(c + 2, d) = 4 OR board(c + 2, d) = 5 OR board(c + 2, d) = 6 THEN
                                board(c + 1, d) = 0
                                blp = blp - 1
                                casn = casn + 1
                                casualx(casn) = c + 2
                                casualy(casn) = d + 1
                        END IF
                END IF
        END IF
        IF (c - 1) > 0 THEN
                IF board(c - 1, d) = 2 AND (c - 2) > -1 THEN
                        IF board(c - 2, d) = 1 OR board(c - 2, d) = 4 OR board(c - 2, d) = 5 OR board(c - 2, d) = 6 THEN
                                board(c - 1, d) = 0
                                blp = blp - 1
                                casn = casn + 1
                                casualx(casn) = c
                                casualy(casn) = d + 1
                        END IF
                END IF
        END IF
END IF
IF colour = 2 AND board(c, d) <> 3 AND board(c, d) <> 6 THEN
        IF (d + 1) < 11 THEN
                IF board(c, d + 1) = 1 AND (d + 2) < 11 THEN
                        IF board(c, d + 2) = 2 OR board(c, d + 2) = 4 OR board(c, d + 2) = 5 OR board(c, d + 2) = 6 THEN
                                board(c, d + 1) = 0
                                whp = whp - 1
                                casn = casn + 1
                                casualx(casn) = c + 1
                                casualy(casn) = d + 2
                        END IF
                END IF
        END IF
        IF (d - 1) > 0 THEN
                IF board(c, d - 1) = 1 AND (d - 2) > -1 THEN
                        IF board(c, d - 2) = 2 OR board(c, d - 2) = 4 OR board(c, d - 2) = 5 OR board(c, d - 2) = 6 THEN
                                board(c, d - 1) = 0
                                whp = whp - 1
                                casn = casn + 1
                                casualx(casn) = c + 1
                                casualy(casn) = d
                        END IF
                END IF
        END IF
        IF (c + 1) < 11 THEN
                IF board(c + 1, d) = 1 AND (c + 2) < 11 THEN
                        IF board(c + 2, d) = 2 OR board(c + 2, d) = 4 OR board(c + 2, d) = 5 OR board(c + 2, d) = 6 THEN
                                board(c + 1, d) = 0
                                whp = whp - 1
                                casn = casn + 1
                                casualx(casn) = c + 2
                                casualy(casn) = d + 1
                        END IF
                END IF
        END IF
        IF (c - 1) > 0 THEN
                IF board(c - 1, d) = 1 AND (c - 2) > -1 THEN
                        IF board(c - 2, d) = 2 OR board(c - 2, d) = 4 OR board(c - 2, d) = 5 OR board(c - 2, d) = 6 THEN
                                board(c - 1, d) = 0
                                whp = whp - 1
                                casn = casn + 1
                                casualx(casn) = c
                                casualy(casn) = d + 1
                        END IF
                END IF
        END IF
END IF
IF colour = 2 THEN
        colour = 1
ELSE
        colour = 2
END IF
IF whp = 0 THEN win = 2
IF blp = 0 THEN
        kingmove = 4
        IF kingx + 1 < 11 THEN
                IF board(kingx + 1, kingy) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingx - 1 > -1 THEN
                IF board(kingx - 1, kingy) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingy + 1 < 11 THEN
                IF board(kingx, kingy + 1) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingy - 1 > -1 THEN
                IF board(kingx, kingy - 1) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingmove < 1 THEN
                win = 3
        END IF
END IF
IF la(0) = lc(2) AND lb(0) = ld(2) AND lc(2) = la(4) AND ld(2) = lb(4) AND la(4) = lc(6) AND lb(4) = ld(6) AND lc(6) = la(8) AND ld(6) = lb(8) THEN
        IF la(1) = lc(3) AND lb(1) = ld(3) AND lc(3) = la(5) AND ld(3) = lb(5) AND la(5) = lc(7) AND lb(5) = ld(7) AND lc(7) = la(9) AND ld(7) = lb(9) THEN
                IF win = 0 THEN
                        win = 3
                END IF
        END IF
END IF
IF board(0, 0) = 3 OR board(0, 10) = 3 OR board(10, 0) = 3 OR board(10, 10) = 3 THEN win = 2
IF kingx + 1 < 11 AND kingx - 1 > 0 AND kingy + 1 < 11 AND kingy - 1 > 0 THEN
        IF board(kingx + 1, kingy) = 1 OR board(kingx + 1, kingy) = 5 THEN
                IF board(kingx - 1, kingy) = 1 OR board(kingx - 1, kingy) = 5 THEN
                        IF board(kingx, kingy + 1) = 1 OR board(kingx, kingy + 1) = 5 THEN
                                IF board(kingx, kingy - 1) = 1 OR board(kinx, kingy - 1) = 5 THEN
                                        win = 1
                                END IF
                        END IF
                END IF
        END IF
END IF
IF blp = 0 AND win = 0 THEN
        kingmove = 4
        IF kingx + 1 < 11 THEN
                IF board(kingx + 1, kingy) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingx - 1 > -1 THEN
                IF board(kingx - 1, kingy) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingy + 1 < 11 THEN
                IF board(kingx, kingy + 1) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingy - 1 > -1 THEN
                IF board(kingx, kingy - 1) = 1 THEN kingmove = kingmove - 1
        ELSE
                kingmove = kingmove - 1
        END IF
        IF kingmove < 1 THEN
                win = 3
        END IF
END IF
END SUB

SUB Choice (board!(), mist!, a!, b!, c!, d!, colour!, win!, kingx, kingy, blp, whp, knight, escape)
a = 0
b = 0
c = 0
d = 0
escape = 0
mist = 1
WHILE mist = 1 AND escape = 0
        Drawboard board(), blp, whp, la, lb, lc, ld, casualx(), casualy(), casn
        PRINT
        IF colour = 2 AND win = 0 THEN
                PRINT "Black's move:-- ";
                IF knight = 0 OR knight = 1 THEN
                        PRINT "Click left mouse button on piece to move     (right button for options)"
                        glump = 0
                        mouseshow
                        DO WHILE glump = 0
                                MouseDriver 3, bx%, CX%, DX%, lb%, RB%, 0
                                coa = CX%
                                cob = DX%
                                IF RB% = -1 THEN
                                        escape = -1
                                        glump = 1
                                END IF
                                FOR countch = 0 TO 10
                                        IF lb% = -1 AND coa = 32 + (countch * 24) AND coa > 31 AND coa < 273 AND cob > 23 AND cob < 105 THEN
                                                a = countch
                                                b = (cob / 8) - 3
                                                glump = 1
                                        END IF
                                NEXT countch
                        LOOP
                        mousehide
                        IF escape = 0 THEN
                                PRINT "Move to?"
                        END IF
                        glump = 0
                        DO UNTIL lb% = 0
                                MouseDriver 3, bx%, CX%, DX%, lb%, RB%, 0
                        LOOP
                        mouseshow
                        DO WHILE glump = 0
                                MouseDriver 3, bx%, CX%, DX%, lb%, RB%, 0
                                coc = CX%
                                cod = DX%
                                IF RB% = -1 THEN
                                        escape = -1
                                        glump = 1
                                END IF
                                FOR countch = 0 TO 10
                                        IF lb% = -1 AND coc = 32 + (countch * 24) AND coc > 31 AND coc < 273 AND cod > 23 AND cod < 105 THEN
                                                c = countch
                                                d = (cod / 8) - 3
                                                glump = 1
                                        END IF
                                NEXT countch
                        LOOP
                        mousehide
                ELSE
                        compblack2 board(), a, b, c, d, mist, kingx, kingy
                END IF
        ELSEIF colour = 1 AND win = 0 THEN
                PRINT "White's move:-- ";
                IF knight = 0 OR knight = 2 THEN
                        PRINT "Click left mouse button on piece to move     (right button for options)"
                        glump = 0
                        mouseshow
                        DO WHILE glump = 0
                                MouseDriver 3, bx%, CX%, DX%, lb%, RB%, 0
                                coa = CX%
                                cob = DX%
                                IF RB% = -1 THEN
                                        escape = -1
                                        glump = 1
                                END IF
                                FOR countch = 0 TO 10
                                        IF lb% = -1 AND coa = 32 + (countch * 24) AND coa > 31 AND coa < 273 AND cob > 23 AND cob < 105 THEN
                                                a = countch
                                                b = (cob / 8) - 3
                                                glump = 1
                                        END IF
                                NEXT countch
                        LOOP
                        mousehide
                        IF escape = 0 THEN
                                PRINT "Move to?"
                        END IF
                        glump = 0
                        DO UNTIL lb% = 0
                                MouseDriver 3, bx%, CX%, DX%, lb%, RB%, 0
                        LOOP
                        mouseshow
                        DO WHILE glump = 0
                                MouseDriver 3, bx%, CX%, DX%, lb%, RB%, 0
                                coc = CX%
                                cod = DX%
                                IF RB% = -1 THEN
                                        escape = -1
                                        glump = 1
                                END IF
                                FOR countch = 0 TO 10
                                        IF lb% = -1 AND coc = 32 + (countch * 24) AND coc > 31 AND coc < 273 AND cod > 23 AND cod < 105 THEN
                                                c = countch
                                                d = (cod / 8) - 3
                                                glump = 1
                                        END IF
                                NEXT countch
                        LOOP
                        mousehide
                ELSE
                        compwhite2 board(), a, b, c, d, mist, kingx, kingy
                END IF
        END IF
        IF escape = 0 THEN move board(), mist, a, b, c, d, colour, win, kingx, kingy
        IF knight = 3 THEN
                PRINT "Hit q to quit or return to continue:"
                INPUT quitty$
                IF quitty$ = "q" OR quitty$ = "Q" THEN END
        END IF
WEND
IF escape = 0 THEN
        FOR i = 9 TO 1 STEP -1
                la(i) = la(i - 1)
                lb(i) = lb(i - 1)
                lc(i) = lc(i - 1)
                ld(i) = ld(i - 1)
        NEXT i
        la(0) = a + 1
        lb(0) = b + 1
        lc(0) = c + 1
        ld(0) = d + 1
        Change board!(), mist!, a!, b!, c!, d!, colour!, win!, kingx, kingy, blp, whp
END IF
END SUB

SUB compblack (board(), a, b, c, d, mist, kingx, kingy)
count = 0
moves = 100
DIM topa(moves)
DIM topb(moves)
DIM topc(moves)
DIM topd(moves)
DIM topn(moves)
topn(0) = -1
FOR i = 0 TO 10
        FOR j = 0 TO 10
                IF board(i, j) = 2 OR board(i, j) = 3 OR board(i, j) = 6 THEN
                        a = i
                        b = j
                        FOR g = 0 TO 10
                                c = a
                                d = g
                                move board(), mist, a, b, c, d, colour, win, kingx, kingy
                                IF mist = 0 THEN
                                        score = 0
                                        IF (c + 1) < 10 THEN
                                                IF board(c + 1, d) = 1 AND board(a, b) = 2 THEN
                                                        IF (c + 2) < 11 THEN
                                                                IF board(c + 2, d) = 2 OR board(c + 2, d) = 4 OR board(c + 2, d) = 5 OR board(c + 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (c - 1) > 0 THEN
                                                IF board(c - 1, d) = 1 AND board(a, b) = 2 THEN
                                                        IF (c - 2) > -1 THEN
                                                                IF board(c - 2, d) = 2 OR board(c - 2, d) = 4 OR board(c - 2, d) = 5 OR board(c - 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d + 1) < 10 THEN
                                                IF board(c, d + 1) = 1 AND board(a, b) = 2 THEN
                                                        IF (d + 2) < 11 THEN
                                                                IF board(c, d + 2) = 2 OR board(c, d + 2) = 4 OR board(c, d + 2) = 5 OR board(c, d + 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d - 1) > 0 THEN
                                                IF board(c, d - 1) = 1 AND board(a, b) = 2 THEN
                                                        IF (d - 2) > -1 THEN
                                                                IF board(c, d - 2) = 2 OR board(c, d - 2) = 4 OR board(c, d - 2) = 5 OR board(c, d - 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        q = 0
                                        WHILE (count + 1) > q
                                                IF topn(q) < score THEN
                                                        topn(q) = score
                                                        topa(q) = a
                                                        topb(q) = b
                                                        topc(q) = c
                                                        topd(q) = d
                                                        q = count + 1
                                                END IF
                                                q = q + 1
                                        WEND
                                        IF score = topn(count) THEN
                                                IF count < (moves) THEN
                                                        count = count + 1
                                                        topn(count) = score
                                                        topa(count) = a
                                                        topb(count) = b
                                                        topc(count) = c
                                                        topd(count) = d
                                                END IF
                                        END IF
                                END IF
                        NEXT g
                        FOR h = 0 TO 10
                                c = h
                                d = b
                                move board(), mist, a, b, c, d, colour, win, kingx, kingy
                                IF mist = 0 THEN
                                        score = 0
                                        IF (c + 1) < 10 THEN
                                                IF board(c + 1, d) = 1 AND board(a, b) = 2 THEN
                                                        IF (c + 2) < 11 THEN
                                                                IF board(c + 2, d) = 2 OR board(c + 2, d) = 4 OR board(c + 2, d) = 5 OR board(c + 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (c - 1) > 0 THEN
                                                IF board(c - 1, d) = 1 AND board(a, b) = 2 THEN
                                                        IF (c - 2) > -1 THEN
                                                                IF board(c - 2, d) = 2 OR board(c - 2, d) = 4 OR board(c - 2, d) = 5 OR board(c - 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d + 1) < 10 THEN
                                                IF board(c, d + 1) = 1 AND board(a, b) = 2 THEN
                                                        IF (d + 2) < 11 THEN
                                                                IF board(c, d + 2) = 2 OR board(c, d + 2) = 4 OR board(c, d + 2) = 5 OR board(c, d + 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d - 1) > 0 THEN
                                                IF board(c, d - 1) = 1 AND board(a, b) = 2 THEN
                                                        IF (d - 2) > -1 THEN
                                                                IF board(c, d - 2) = 2 OR board(c, d - 2) = 4 OR board(c, d - 2) = 5 OR board(c, d - 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        q = 0
                                        WHILE (count + 1) > q
                                                IF topn(q) < score THEN
                                                        topn(q) = score
                                                        topa(q) = a
                                                        topb(q) = b
                                                        topc(q) = c
                                                        topd(q) = d
                                                        q = count + 1
                                                END IF
                                                q = q + 1
                                        WEND
                                        IF score = topn(count) THEN
                                                IF count < (moves) THEN
                                                        count = count + 1
                                                        topn(count) = score
                                                        topa(count) = a
                                                        topb(count) = b
                                                        topc(count) = c
                                                        topd(count) = d
                                                END IF
                                        END IF
                                END IF
                        NEXT h
                END IF
        NEXT j
NEXT i
DIM cmove(moves)
cc = 0
q = 0
WHILE count > q
        IF cmove(cc) < topn(q) THEN
                cmove(cc) = topn(q)
        ELSEIF cmove(cc) = topn(q) THEN
                IF cc < moves THEN
                        cc = cc + 1
                        cmove(cc) = topn(q)
                END IF
        END IF
        q = q + 1
WEND
RANDOMIZE TIMER
rt% = INT(RND * cc)
a = topa(rt%)
b = topb(rt%)
c = topc(rt%)
d = topd(rt%)
END SUB

SUB compblack2 (board(), a, b, c, d, mist, kingx, kingy)
count = -1
rcount = -1
moves = 100
DIM topa(moves)
DIM topb(moves)
DIM topc(moves)
DIM topd(moves)
DIM topn(moves)
FOR i = 0 TO 1000
        routx(i) = 0
        routy(i) = 0
NEXT i
faill = 0
FOR xyz = 0 TO 3
IF xyz = 0 THEN
        tarx = 0
        tary = 0
ELSEIF xyz = 1 THEN
        tarx = 10
        tary = 0
ELSEIF xyz = 2 THEN
        tarx = 0
        tary = 10
ELSE
        tarx = 10
        tary = 10
END IF
j = tarx + 1
WHILE faill = 0
        IF j < 11 THEN
                IF board(j, tary) = 3 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = j
                        topb(count) = tary
                        topc(count) = tarx
                        topd(count) = tary
                        faill = 1
                ELSEIF board(j, tary) = 2 OR board(j, tary) = 1 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j + 1
        IF j > 10 THEN faill = 1
WEND
faill = 0
j = tarx - 1
WHILE faill = 0
        IF j > -1 THEN
                IF board(j, tary) = 3 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = j
                        topb(count) = tary
                        topc(count) = tarx
                        topd(count) = tary
                        faill = 1
                ELSEIF board(j, tary) = 2 OR board(j, tary) = 1 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j - 1
        IF j < 0 THEN faill = 1
WEND
faill = 0
j = tary + 1
WHILE faill = 0
        IF j < 11 AND faill = 0 THEN
                IF board(tarx, j) = 3 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = tarx
                        topb(count) = j
                        topc(count) = tarx
                        topd(count) = tary
                        faill = 1
                ELSEIF board(tarx, j) = 2 OR board(tarx, j) = 1 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j + 1
        IF j > 10 THEN faill = 1
WEND
faill = 0
j = tary - 1
WHILE faill = 0
        IF j > -1 THEN
                IF board(tarx, j) = 3 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = tarx
                        topb(count) = j
                        topc(count) = tarx
                        topd(count) = tary
                        faill = 1
                ELSEIF board(tarx, j) = 2 OR board(tarx, j) = 1 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j - 1
        IF j < 0 THEN faill = 1
WEND
IF count < 0 THEN
        faill = 0
        i = tarx + 1
        WHILE faill = 0
                IF i < 11 THEN
                        IF board(i, tary) = 0 THEN
                                edgemove board(), i, tary, path, 0
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = tarx + 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = i
                                        topb(count) = path
                                        topc(count) = i
                                        topd(count) = tary
                                END IF
                                i = i + 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = tarx - 1
        WHILE faill = 0
                IF i > -1 THEN
                        IF board(i, tary) = 0 THEN
                                edgemove board(), i, tary, path, 0
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = tarx - 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = i
                                        topb(count) = path
                                        topc(count) = i
                                        topd(count) = tary
                                END IF
                                i = i - 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = tary + 1
        WHILE faill = 0
                IF i < 11 THEN
                        IF board(tarx, i) = 0 THEN
                                edgemove board(), tarx, i, path, 1
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = tary + 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = path
                                        topb(count) = i
                                        topc(count) = tarx
                                        topd(count) = i
                                END IF
                                i = i + 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = tary - 1
        WHILE faill = 0
                IF i > -1 THEN
                        IF board(tarx, i) = 0 THEN
                                edgemove board(), tarx, i, path, 1
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = tary - 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = path
                                        topb(count) = i
                                        topc(count) = tarx
                                        topd(count) = i
                                END IF
                                i = i - 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
END IF
NEXT xyz
IF count < 0 AND blevel > 1 THEN
FOR i = 0 TO rcount
        move board(), mist, kingx, kingy, routx(i), routy(i), colour, win, kingx, kingy
        IF mist = 0 THEN
                count = count + 1
                topn(count) = 95
                topa(count) = kingx
                topb(count) = kingy
                topc(count) = routx(i)
                topd(count) = routy(i)
        END IF
NEXT i
END IF
IF count < 0 THEN
        runfast = 0
        IF (kingx + 1) < 11 THEN
                IF board((kingx + 1), kingy) = 1 OR board((kingx + 1), kingy) = 5 THEN runfast = runfast + 1
        END IF
        IF (kingx - 1) > -1 THEN
                IF board((kingx - 1), kingy) = 1 OR board((kingx - 1), kingy) = 5 THEN runfast = runfast + 1
        END IF
        IF (kingy + 1) < 11 THEN
                IF board(kingx, (kingy + 1)) = 1 OR board(kingx, (kingy + 1)) = 5 THEN runfast = runfast + 1
        END IF
        IF (kingy - 1) > -1 THEN
                IF board(kingx, (kingy - 1)) = 1 OR board(kingx, (kingy - 1)) = 5 THEN runfast = runfast + 1
        END IF
        IF runfast > 0 THEN
        faill = 0
        i = kingx + 1
        WHILE faill = 0
                IF i < 11 THEN
                        IF board(i, kingy) = 0 OR board(i, kingy) = 5 THEN
                                safe = 0
                                IF (i + 1) < 11 THEN
                                        IF board((i + 1), kingy) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (i - 1) > -1 THEN
                                        IF board((i - 1), kingy) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingy + 1) < 11 THEN
                                        IF board(i, (kingy + 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingy - 1) > -1 THEN
                                        IF board(i, (kingy - 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF safe < runfast THEN
                                        count = count + 1
                                        topn(count) = 90
                                        topa(count) = kingx
                                        topb(count) = kingy
                                        topc(count) = i
                                        topd(count) = kingy
                                END IF
                                i = i + 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = kingx - 1
        WHILE faill = 0
                IF i > -1 THEN
                        IF board(i, kingy) = 0 OR board(i, kingy) = 5 THEN
                                safe = 0
                                IF (i + 1) < 11 THEN
                                        IF board((i + 1), kingy) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (i - 1) > -1 THEN
                                        IF board((i - 1), kingy) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingy + 1) < 11 THEN
                                        IF board(i, (kingy + 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingy - 1) > -1 THEN
                                        IF board(i, (kingy - 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF safe < runfast THEN
                                        count = count + 1
                                        topn(count) = 90
                                        topa(count) = kingx
                                        topb(count) = kingy
                                        topc(count) = i
                                        topd(count) = kingy
                                END IF
                                i = i - 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = kingy + 1
        WHILE faill = 0
                IF i < 11 THEN
                        IF board(kingx, i) = 0 OR board(kingx, i) = 5 THEN
                                safe = 0
                                IF (i + 1) < 11 THEN
                                        IF board(kingx, (i + 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (i - 1) > -1 THEN
                                        IF board(kingx, (i - 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingx + 1) < 11 THEN
                                        IF board((kingx + 1), i) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingx - 1) > -1 THEN
                                        IF board((kingx - 1), i) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF safe < runfast THEN
                                        count = count + 1
                                        topn(count) = 90
                                        topa(count) = kingx
                                        topb(count) = kingy
                                        topc(count) = kingx
                                        topd(count) = i
                                END IF
                                i = i + 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = kingy - 1
        WHILE faill = 0
                IF i > -1 THEN
                        IF board(kingx, i) = 0 OR board(kingx, i) = 5 THEN
                                safe = 0
                                IF (i + 1) < 11 THEN
                                        IF board(kingx, (i + 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (i - 1) > -1 THEN
                                        IF board(kingx, (i - 1)) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingx + 1) < 11 THEN
                                        IF board((kingx + 1), i) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF (kingx - 1) > -1 THEN
                                        IF board((kingx - 1), i) = 1 THEN
                                                safe = safe + 1
                                        END IF
                                END IF
                                IF safe < runfast THEN
                                        count = count + 1
                                        topn(count) = 90
                                        topa(count) = kingx
                                        topb(count) = kingy
                                        topc(count) = kingx
                                        topd(count) = i
                                END IF
                                i = i - 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        END IF
END IF


jkl = 0
IF count > -1 THEN
        DIM store2(count)
        temp = 0
        f = -1
        FOR v = 0 TO count
                IF topn(v) > 100 THEN
                        f = f + 1
                        store2(f) = v
                END IF
        NEXT v
        RANDOMIZE TIMER
        IF f > -1 THEN
                rt% = INT(RND * f)
                jkl = store2(rt%)
        ELSE
                rt% = INT(RND * count)
                jkl = rt%
        END IF
        a = topa(jkl)
        b = topb(jkl)
        c = topc(jkl)
        d = topd(jkl)
ELSE
        compblack board(), a, b, c, d, mist, kingx, kingy
END IF
END SUB

SUB compwhite (board(), a, b, c, d, mist, kingx, kingy)
count = 0
moves = 100
DIM topa(moves)
DIM topb(moves)
DIM topc(moves)
DIM topd(moves)
DIM topn(moves)
topn(0) = -1
FOR i = 0 TO 10
        FOR j = 0 TO 10
                IF board(i, j) = 1 THEN
                        a = i
                        b = j
                        FOR g = 0 TO 10
                                c = a
                                d = g
                                move board(), mist, a, b, c, d, colour, win, kingx, kingy
                                IF mist = 0 THEN
                                        IF a - 1 > -1 THEN
                                                IF board((a - 1), b) = 3 OR board((a - 1), b) = 6 THEN score = score - 95
                                        END IF
                                        IF a + 1 < 11 THEN
                                                IF board((a + 1), b) = 3 OR board((a + 1), b) = 6 THEN score = score - 95
                                        END IF
                                        IF b - 1 > -1 THEN
                                                IF board(a, (b - 1)) = 3 OR board(a, (b - 1)) = 6 THEN score = score - 95
                                        END IF
                                        IF b + 1 < 11 THEN
                                                IF board(a, (b + 1)) = 3 OR board(a, (b + 1)) = 6 THEN score = score - 95
                                        END IF
                                        score = 0
                                        IF wlevel > 1 THEN
                                                FOR mno = 0 TO rcount
                                                        IF c = routx(mno) AND d = routy(mno) THEN
                                                                score = score + 95
                                                        END IF
                                                NEXT mno
                                        END IF
                                        IF (c + 1) < 10 THEN
                                                IF board(c + 1, d) = 2 THEN
                                                        IF (c + 2) < 11 THEN
                                                                IF board(c + 2, d) = 1 OR board(c + 2, d) = 4 OR board(c + 2, d) = 5 OR board(c + 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (c - 1) > 0 THEN
                                                IF board(c - 1, d) = 2 THEN
                                                        IF (c - 2) > -1 THEN
                                                                IF board(c - 2, d) = 1 OR board(c - 2, d) = 4 OR board(c - 2, d) = 5 OR board(c - 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d + 1) < 10 THEN
                                                IF board(c, d + 1) = 2 THEN
                                                        IF (d + 2) < 11 THEN
                                                                IF board(c, d + 2) = 1 OR board(c, d + 2) = 4 OR board(c, d + 2) = 5 OR board(c, d + 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d - 1) > 0 THEN
                                                IF board(c, d - 1) = 2 THEN
                                                        IF (d - 2) > -1 THEN
                                                                IF board(c, d - 2) = 1 OR board(c, d - 2) = 4 OR board(c, d - 2) = 5 OR board(c, d - 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        q = 0
                                        WHILE (count + 1) > q
                                                IF topn(q) < score THEN
                                                        topn(q) = score
                                                        topa(q) = a
                                                        topb(q) = b
                                                        topc(q) = c
                                                        topd(q) = d
                                                        q = count + 1
                                                END IF
                                                q = q + 1
                                        WEND
                                        IF score = topn(count) THEN
                                                IF count < (moves) THEN
                                                        count = count + 1
                                                        topn(count) = score
                                                        topa(count) = a
                                                        topb(count) = b
                                                        topc(count) = c
                                                        topd(count) = d
                                                END IF
                                        END IF
                                END IF
                        NEXT g
                        FOR h = 0 TO 10
                                c = h
                                d = b
                                move board(), mist, a, b, c, d, colour, win, kingx, kingy
                                IF mist = 0 THEN
                                        score = 0
                                        IF wlevel > 1 THEN
                                                FOR mno = 0 TO rcount
                                                        IF c = routx(mno) AND d = routy(mno) THEN
                                                                score = score + 95
                                                        END IF
                                                NEXT mno
                                        END IF
                                        IF (c + 1) < 10 THEN
                                                IF board(c + 1, d) = 2 THEN
                                                        IF (c + 2) < 11 THEN
                                                                IF board(c + 2, d) = 1 OR board(c + 2, d) = 4 OR board(c + 2, d) = 5 OR board(c + 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (c - 1) > 0 THEN
                                                IF board(c - 1, d) = 2 THEN
                                                        IF (c - 2) > -1 THEN
                                                                IF board(c - 2, d) = 1 OR board(c - 2, d) = 4 OR board(c - 2, d) = 5 OR board(c - 2, d) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d + 1) < 10 THEN
                                                IF board(c, d + 1) = 2 THEN
                                                        IF (d + 2) < 11 THEN
                                                                IF board(c, d + 2) = 1 OR board(c, d + 2) = 4 OR board(c, d + 2) = 5 OR board(c, d + 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        IF (d - 1) > 0 THEN
                                                IF board(c, d - 1) = 2 THEN
                                                        IF (d - 2) > -1 THEN
                                                                IF board(c, d - 2) = 1 OR board(c, d - 2) = 4 OR board(c, d - 2) = 5 OR board(c, d - 2) = 6 THEN
                                                                        score = score + 10
                                                                END IF
                                                        END IF
                                                        score = score + 1
                                                END IF
                                        END IF
                                        q = 0
                                        WHILE (count + 1) > q
                                                IF topn(q) < score THEN
                                                        topn(q) = score
                                                        topa(q) = a
                                                        topb(q) = b
                                                        topc(q) = c
                                                        topd(q) = d
                                                        q = count + 1
                                                END IF
                                                q = q + 1
                                        WEND
                                        IF score = topn(count) THEN
                                                IF count < (moves) THEN
                                                        count = count + 1
                                                        topn(count) = score
                                                        topa(count) = a
                                                        topb(count) = b
                                                        topc(count) = c
                                                        topd(count) = d
                                                END IF
                                        END IF
                                END IF
                        NEXT h
                END IF
        NEXT j
NEXT i
DIM cmove(moves)
cc = 0
q = 0
WHILE count > q
        IF cmove(cc) < topn(q) THEN
                cmove(cc) = topn(q)
        ELSEIF cmove(cc) = topn(q) THEN
                IF cc < moves THEN
                        cc = cc + 1
                        cmove(cc) = topn(q)
                END IF
        END IF
        q = q + 1
WEND
RANDOMIZE TIMER
rt% = INT(RND * cc)
a = topa(rt%)
b = topb(rt%)
c = topc(rt%)
d = topd(rt%)
END SUB

SUB compwhite2 (board(), a, b, c, d, mist, kingx, kingy)
count = -1
rcount = -1
moves = 100
DIM topa(moves)
DIM topb(moves)
DIM topc(moves)
DIM topd(moves)
DIM topn(moves)
FOR i = 0 TO 1000
        routx(i) = 0
        routy(i) = 0
NEXT i
faill = 0
i = kingx + 1
j = kingx + 2
WHILE faill = 0
        IF i < 11 THEN
                IF board(i, kingy) = 1 OR board(i, kingy) = 2 OR board(i, kingy) = 5 THEN
                        faill = 1
                END IF
        END IF
        IF j < 11 AND faill = 0 THEN
                IF board(j, kingy) = 1 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = j
                        topb(count) = kingy
                        topc(count) = i
                        topd(count) = kingy
                        faill = 1
                ELSEIF board(j, kingy) = 2 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j + 1
        IF j > 10 THEN faill = 1
WEND
faill = 0
i = kingx - 1
j = kingx - 2
WHILE faill = 0
        IF i > -1 THEN
                IF board(i, kingy) = 1 OR board(i, kingy) = 2 OR board(i, kingy) = 5 THEN
                        faill = 1
                END IF
        END IF
        IF j > -1 AND faill = 0 THEN
                IF board(j, kingy) = 1 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = j
                        topb(count) = kingy
                        topc(count) = i
                        topd(count) = kingy
                        faill = 1
                ELSEIF board(j, kingy) = 2 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j - 1
        IF j < 0 THEN faill = 1
WEND
faill = 0
i = kingy + 1
j = kingy + 2
WHILE faill = 0
        IF i < 11 THEN
                IF board(kingx, i) = 1 OR board(kingx, i) = 2 OR board(kingx, i) = 5 THEN
                        faill = 1
                END IF
        END IF
        IF j < 11 AND faill = 0 THEN
                IF board(kingx, j) = 1 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = kingx
                        topb(count) = j
                        topc(count) = kingx
                        topd(count) = i
                        faill = 1
                ELSEIF board(kingx, j) = 2 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j + 1
        IF j > 10 THEN faill = 1
WEND
faill = 0
i = kingy - 1
j = kingy - 2
WHILE faill = 0
        IF i > -1 THEN
                IF board(kingx, i) = 1 OR board(kingx, i) = 2 OR board(kingx, i) = 5 THEN
                        faill = 1
                END IF
        END IF
        IF j > -1 AND faill = 0 THEN
                IF board(kingx, j) = 1 THEN
                        count = count + 1
                        topn(count) = 1000
                        topa(count) = kingx
                        topb(count) = j
                        topc(count) = kingx
                        topd(count) = i
                        faill = 1
                ELSEIF board(kingx, j) = 2 THEN
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
        j = j - 1
        IF j < 0 THEN faill = 1
WEND

        faill = 0
        i = kingx + 1
        WHILE faill = 0
                IF i < 11 THEN
                        IF board(i, kingy) = 5 THEN
                                i = i + 1
                        ELSEIF board(i, kingy) = 0 THEN
                                attack board(), i, kingy, path, 0
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = kingx + 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = i
                                        topb(count) = path
                                        topc(count) = i
                                        topd(count) = kingy
                                END IF
                                i = i + 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = kingx - 1
        WHILE faill = 0
                IF i > -1 THEN
                        IF board(i, kingy) = 5 THEN
                                i = i - 1
                        ELSEIF board(i, kingy) = 0 THEN
                                attack board(), i, kingy, path, 0
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = kingx - 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = i
                                        topb(count) = path
                                        topc(count) = i
                                        topd(count) = kingy
                                END IF
                                i = i - 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = kingy + 1
        WHILE faill = 0
                IF i < 11 THEN
                        IF board(kingx, i) = 5 THEN
                                i = i + 1
                        ELSEIF board(kingx, i) = 0 THEN
                                attack board(), kingx, i, path, 1
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = kingy + 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = path
                                        topb(count) = i
                                        topc(count) = kingx
                                        topd(count) = i
                                END IF
                                i = i + 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND
        faill = 0
        i = kingy - 1
        WHILE faill = 0
                IF i > -1 THEN
                        IF board(kingx, i) = 5 THEN
                                i = i - 1
                        ELSEIF board(kingx, i) = 0 THEN
                                attack board(), kingx, i, path, 1
                                IF path > -1 THEN
                                        count = count + 1
                                        IF i = kingy - 1 THEN
                                                topn(count) = 1000
                                        ELSE
                                                topn(count) = 100
                                        END IF
                                        topa(count) = path
                                        topb(count) = i
                                        topc(count) = kingx
                                        topd(count) = i
                                END IF
                                i = i - 1
                        ELSE
                                faill = 1
                        END IF
                ELSE
                        faill = 1
                END IF
        WEND

jkl = 0
IF count > -1 THEN
        DIM store2(count)
        temp = 0
        f = -1
        FOR v = 0 TO count
                IF topn(v) > 100 THEN
                        f = f + 1
                        store2(f) = v
                END IF
        NEXT v
        RANDOMIZE TIMER
        IF f > -1 THEN
                rt% = INT(RND * f)
                jkl = store2(rt%)
        ELSE
                rt% = INT(RND * count)
                jkl = rt%
        END IF
        a = topa(jkl)
        b = topb(jkl)
        c = topc(jkl)
        d = topd(jkl)
ELSE
        compwhite board(), a, b, c, d, mist, kingx, kingy
END IF
END SUB

SUB Drawboard (board(), blp, whp, la, lb, lc, ld, casualx(), casualy(), casn)
CLS
COLOR 15
y = 0
x = 0
PRINT "                              CYNINGTAFL (1.0)"
PRINT
COLOR 1
PRINT "    1  2  3  4  5  6  7  8  9  10 11"
FOR y = 0 TO 10
        COLOR 1
        IF y < 9 THEN
                PRINT y + 1;
                PRINT " ";
        ELSE
                PRINT y + 1;
        END IF
        FOR x = 0 TO 10
                IF board(x, y) = 0 THEN
                        COLOR 2
                        PRINT "-  ";
                END IF
                IF board(x, y) = 1 THEN
                        COLOR 7
                        PRINT "#  ";
                END IF
                IF board(x, y) = 2 THEN
                        COLOR 4
                        PRINT "@  ";
                END IF
                IF board(x, y) = 3 THEN
                        COLOR 4
                        PRINT "!  ";
                END IF
                IF board(x, y) = 4 THEN
                        COLOR 10
                        PRINT "x  ";
                END IF
                IF board(x, y) = 5 THEN
                        COLOR 10
                        PRINT "x  ";
                END IF
                IF board(x, y) = 6 THEN
                        COLOR 4
                        PRINT "?  ";
                END IF
        NEXT x
        COLOR 1
        IF y < 9 THEN
                PRINT y + 1;
                PRINT ;
        ELSE
                PRINT y + 1;
        END IF
        COLOR 1
        IF y = 0 THEN
                PRINT "    ";
                COLOR 2
                PRINT "-";
                COLOR 1
                PRINT " = Empty Space";
        END IF
        IF y = 2 THEN
                PRINT "    ";
                COLOR 7
                PRINT "#";
                COLOR 1
                PRINT " = White Piece";
        END IF
        IF y = 3 THEN
                PRINT "   ";
                COLOR 4
                PRINT " @";
                COLOR 1
                PRINT " = Black Piece";
        END IF
        IF y = 1 THEN
                PRINT "    ";
                COLOR 10
                PRINT "x";
                COLOR 1
                PRINT " = Centre or Corner 'Dark' Square";
        END IF
        IF y = 4 THEN
                PRINT "    ";
                COLOR 4
                PRINT "?";
                COLOR 1
                PRINT " = Black King on Centre Square";
        END IF
        IF y = 5 THEN
                PRINT "    ";
                COLOR 4
                PRINT "!";
                COLOR 1
                PRINT " = Black King Not on Centre Square";
        END IF
        IF y = 6 THEN PRINT "";
        COLOR 3
        IF y = 7 THEN PRINT "        # of White Pieces Remaining"; whp;
        COLOR 3
        IF y = 8 THEN PRINT "        # of Black Pieces Remaining"; blp;
        IF y = 9 THEN PRINT "";
        COLOR 3
        IF y = 10 THEN PRINT "   Last Move:"; la(0); ","; lb(0); " to "; lc(0); ","; ld(0);
        
        PRINT
NEXT y
COLOR 1
PRINT "    1  2  3  4  5  6  7  8  9  10 11";
COLOR 3
PRINT "        Captures:"
PRINT "                                              ";
FOR lpc = 0 TO casn
        IF lpc > 0 THEN PRINT " & ";
        PRINT casualx(lpc); ","; casualy(lpc);
NEXT lpc
PRINT
COLOR 7
END SUB

SUB edgemove (board(), x, y, path, dir)
DIM choic(1)
path = -1
faill = 0
count = -1
i = x + 1
WHILE faill = 0
        IF i < 11 AND dir = 1 THEN
                IF board(i, y) = 3 OR board(i, y) = 6 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(i, y) = 0 OR board(i, y) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = i
                        routy(rcount) = y
                        i = i + 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
faill = 0
i = x - 1
WHILE faill = 0
        IF i > -1 AND dir = 1 THEN
                IF board(i, y) = 3 OR board(i, y) = 6 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(i, y) = 0 OR board(i, y) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = i
                        routy(rcount) = y
                        i = i - 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
faill = 0
i = y + 1
WHILE faill = 0
        IF i < 11 AND dir = 0 THEN
                IF board(x, i) = 3 OR board(x, i) = 6 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(x, i) = 0 OR board(x, i) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = x
                        routy(rcount) = i
                        i = i + 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
faill = 0
i = y - 1
WHILE faill = 0
        IF i > -1 AND dir = 0 THEN
                IF board(x, i) = 3 OR board(x, i) = 6 THEN
                        count = count + 1
                        faill = 1
                        choic(count) = i
                ELSEIF board(x, i) = 0 OR board(x, i) = 5 THEN
                        rcount = rcount + 1
                        routx(rcount) = x
                        routy(rcount) = i
                        i = i - 1
                ELSE
                        faill = 1
                END IF
        ELSE
                faill = 1
        END IF
WEND
IF count > -1 THEN
        RANDOMIZE TIMER
        rt% = INT(RND * count)
        temp = rt%
        path = choic(temp)
END IF
END SUB

SUB MouseDriver (AX%, bx%, CX%, DX%, lb%, RB%, EX%)
IF EX% = 1 THEN
mouse$ = SPACE$(57)
FOR i% = 1 TO 57
READ a$
h$ = CHR$(VAL("&H" + a$))
MID$(mouse$, i%, 1) = h$
NEXT i%




CLS
END IF
DEF SEG = VARSEG(mouse$)
CALL Absolute(AX%, bx%, CX%, DX%, SADD(mouse$))
lb% = ((bx% AND 1) <> 0)
RB% = ((bx% AND 2) <> 0)
END SUB

SUB mousehide
 AX% = 2
 MouseDriver AX%, 0, 0, 0, 0, 0, 0
END SUB

SUB mouseshow
 AX% = 1
 MouseDriver AX%, 0, 0, 0, 0, 0, 0
END SUB

SUB move (board(), mist, a, b, c, d, colour, win, kingx, kingy)
mist = 0
IF colour = 1 THEN
        IF board(a, b) <> 1 THEN
                mist = 1
        END IF
END IF
IF colour = 2 THEN
        IF board(a, b) = 0 OR board(a, b) = 1 OR board(a, b) = 4 OR board(a, b) = 5 THEN
                mist = 1
        END IF
END IF
squares = 0
xory = 0
IF a = c THEN
        squares = (b - d)
        xory = xory + 1
END IF
IF b = d THEN
        squares = (a - c)
        xory = xory + 1
END IF
IF xory <> 1 THEN
        mist = 1
END IF
IF board(a, b) <> 3 AND board(c, d) = 5 THEN
        mist = 1
END IF
IF board(a, b) <> 3 AND board(c, d) = 4 THEN
        mist = 1
END IF
count = 0
WHILE count <> squares
        IF a = c THEN
                IF board(c, d + count) <> 0 AND board(c, d + count) <> 5 AND board(c, d + count) <> 4 THEN
                        mist = 1
                END IF
        ELSE
                IF board(c + count, d) <> 0 AND board(c + count, d) <> 5 AND board(c + count, d) <> 4 THEN mist = 1
        END IF
        IF squares < count THEN
                count = count - 1
        ELSE
                count = count + 1
        END IF
WEND
END SUB

