1
00:00:12,768 --> 00:00:16,147
I'm all alone.

2
00:00:17,290 --> 00:00:20,501
In this world,

3
00:00:21,727 --> 00:00:25,997
if I have any friend, it's my shadow

4
00:00:26,271 --> 00:00:29,251
I'm all alone.

5
00:00:30,549 --> 00:00:33,876
In this world,

6
00:00:34,859 --> 00:00:38,905
if I have any friend, it's my shadow

7
00:00:39,508 --> 00:00:42,630
I'm all alone.

8
00:01:05,733 --> 00:01:10,029
I'm not an infatuated moth, nor a crazy fellow

9
00:01:10,414 --> 00:01:14,579
I'm not part of any gathering

10
00:01:14,666 --> 00:01:18,693
emtpy empty paths hold me faroff

11
00:01:19,224 --> 00:01:23,158
who has the sorrow of destination?

12
00:01:23,617 --> 00:01:27,738
I'm not an infatuated moth, nor a crazy fellow

13
00:01:28,153 --> 00:01:31,969
I'm not part of any gathering

14
00:01:32,542 --> 00:01:36,367
empty empty paths hold me faroff

15
00:01:36,686 --> 00:01:40,425
who has the sorrow of destination?

16
00:01:41,218 --> 00:01:44,295
me, I'm a traveller of the heart

17
00:01:44,357 --> 00:01:49,660
if I have any friend, it's my shadow

18
00:01:49,877 --> 00:01:53,389
I'm all alone.

19
00:01:54,283 --> 00:01:57,811
in this world

20
00:01:58,190 --> 00:02:02,707
if I have any friend, it's my shadow

21
00:02:03,309 --> 00:02:06,323
I'm all alone.

22
00:02:25,196 --> 00:02:33,470
like a lonely swan leaving the banks of some lovely lake

23
00:02:33,783 --> 00:02:35,859
look, sir, how

24
00:02:35,859 --> 00:02:38,028
these vagabonds(?)

25
00:02:38,330 --> 00:02:41,961
are travelling on the crests of waves

26
00:02:42,420 --> 00:02:50,481
like a lonely swan leaving the banks of some lovely lake

27
00:02:51,363 --> 00:02:53,033
look, sir, how

28
00:02:53,404 --> 00:02:55,223
these vagabonds(?)

29
00:02:55,682 --> 00:02:59,406
are travelling on the crests of waves

30
00:02:59,814 --> 00:03:02,890
beneath the moon and stars

31
00:03:03,644 --> 00:03:08,164
if I have any friend, then it's my shadow

32
00:03:08,518 --> 00:03:11,945
I'm all alone.

33
00:03:13,315 --> 00:03:16,485
in this world

34
00:03:17,120 --> 00:03:21,218
if I have any friend, then it's my shadow

35
00:03:21,395 --> 00:03:25,716
I'm all alone.

